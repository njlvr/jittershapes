#!/usr/bin/env python
'''
=== crop ===
The viewbox(?) is an arbitrairy shape
Our drawing consistes of lines that cross the viewbox
    0 times
        if 1 endpoint is inside => keep the line
    1 time
        1 endpoint is inside the other one will de moved to the intersection of the box and the license
    2 times
        #   #      ____________       |
        #   | x----\----------/---x   |
        #   |       \________/        |
        #
        #       ____________________
        #   |   \      _______     /      |
        #   |    \  x--\-----/--x /       |
        #   |     \     \___/    /        |
        #   |      \____________/         |
    1st endpoint inside the viewbox
    no
        delete 1st enpoint
    yes
        move on to next intersection
    For each intersection in the right order
    
    if enpoint1 not inside
        start is endpoint1
    else
        start is undef
    foreach point in [ intersection... endpoint2 ]
        # point on the edge of viewbox 
        start != undef
            make segment( start, point )
        else skip
            start = point

# Shapely ?
C:\Program Files\Inkscape\lib\python3.10\site-packages <<= 10
packages\shapely\__init__.py", line 13, in <module>
    from shapely.lib import GEOSException  # NOQA
3) Copy the missing additional DLLs from msys64\bin to Inkscape\ (bin?)
==> you need to ensure to have GEOS installed before pip installing Shapely.
How?

view-source:http://www.kevlindev.com/geometry/2D/polygon-in-out.svg
https://stackoverflow.com/questions/22232311/finding-intersection-between-straight-line-and-contour
https://stackoverflow.com/questions/3252194/numpy-and-line-intersections
https://inkscapetutorial.org/extension-get-started.html
https://pypi.org/project/shapely/
C:\Program Files\Inkscape\lib\python3.10\site-packages
https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/introduction.html
https://packaging.python.org/en/latest/tutorials/installing-packages/   <= recent
'''

'''
Copyright (C) 2012 Juan Pablo Carbajal ajuanpi-dev@gmail.com
Copyright (C) 2005 Aaron Spike, aaron@ekips.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
import random, math, inkex
from inkex import paths
import os
inkex.utils.debug(os.environ)
import shapely.geometry as shgeo
import numpy as np

LEFT = 0
NODE = 1
RIGHT = 2
X = 0
Y = 1
U = 2
V = 3
# TODO
# N.B. No overlap
# Check effet locked layers
# /!\ Shapes of 2 shapes ? Probably no pb
# dist_* => null

def display(tag, node):
    indent = ""
    if tag:
        inkex.utils.debug(tag + ":")
        indent = "    "
    else:
        inkex.utils.debug('')

    inkex.utils.debug('{}node = [{}, {}]'.format(indent, node[X], node[Y]))
 

def idNode( node ):
    # Identify the NODE
    return 'node[{},{}]'.format( node[ X ], node[ Y ] )

def idHandle( prev, node ):
    # Identify the handle
    return [ 'handle[{}-{}]'.format( idNode( prev ), idNode( node )),
             'handle[{}-{}]'.format( idNode( node ), idNode( prev )) ]

# https://www.it-swarm-fr.com/fr/java/comment-determiner-si-un-point-est-linterieur-dun-polygone-convexe-2d/940800366/
def inside( node, subpath, debug ):
    if debug:
        display( "\n===inside\nnode", node )
        inkex.utils.debug('subpath: ' + str(subpath))
        inkex.utils.debug('len(subpath: ' + str(len(subpath)))

    # subpath contains 1st element as last element as well
    result = False
    for i in range( 1, len( subpath )):
        j = i - 1
        if debug:
            inkex.utils.debug('i,j: {},{}: '.format( i, j ))
            inkex.utils.debug('subpath-i: ' + str(subpath[i][NODE]))
            inkex.utils.debug('subpath-j: ' + str(subpath[j][NODE]))
            inkex.utils.debug( "1 = {}".format (subpath[i][NODE][Y] > node[Y]))
            inkex.utils.debug( "2 = {}".format (node[X] ))
        diviseur = subpath[j][NODE][Y] - subpath[i][NODE][Y]
        if debug and diviseur != 0:
            inkex.utils.debug( "diviseur = {}".format ( (subpath[j][NODE][Y] - subpath[i][NODE][Y]) ))
            inkex.utils.debug( "3 = {}".format ((subpath[j][NODE][X] - subpath[i][NODE][X]) * (node[Y] - subpath[i][NODE][Y]) / (subpath[j][NODE][Y] - subpath[i][NODE][Y]) + subpath[i][NODE][X]))
            inkex.utils.debug( "4 = {}".format ( node[X] < (subpath[j][NODE][X] - subpath[i][NODE][X]) * (node[Y] - subpath[i][NODE][Y]) / (subpath[j][NODE][Y] - subpath[i][NODE][Y]) + subpath[i][NODE][X]))

        if (((subpath[i][NODE][Y] > node[Y]) != (subpath[j][NODE][Y] > node[Y]))
                and (node[X] < (subpath[j][NODE][X] -
                                subpath[i][NODE][X]) * (node[Y] - 
                                                        subpath[i][NODE][Y]) /
                                                        (subpath[j][NODE][Y] - subpath[i][NODE][Y]) + 
                                                        subpath[i][NODE][X])):
            result = not result

    if debug:
        inkex.utils.debug('===' + str( result ) + '=========================================')
    return result


class Rogner(inkex.EffectExtension):
    '''Jiggle shapes around'''
    def add_arguments(self, pars):
        pars.add_argument("--debug",
                        type=inkex.Boolean, default=False,
                        help="Debug mode. Ends in i=1/0<br>JZeroDivisionError: division by zero")
        pars.add_argument("--tab",
                        help="The selected UI-tab when OK was pressed")

    def effect(self):
        middle = [ 75, 125 ]
        for id, node in iter(self.svg.selected.items()):
            if node.tag == inkex.addNS('path','svg'):
                d = node.get('d')
                if self.options.debug:
                    inkex.utils.debug('\n*** node: ' + str(node.attrib["id"])) # path9999
                # parsePath:: Parse SVG path and return an array of segments.
                # Removes all shorthand notation.
                # Converts coordinates to absolute. <==
                # p = cubicsuperpath.parsePath(d)
                p = paths.CubicSuperPath(paths.Path(d))
                for subpath in p:
                    inkex.utils.debug('subpath: ' + str(subpath))
                    inside( middle, subpath, self.options.debug )
        
        if self.options.debug:
            i=1/0

if __name__ == '__main__':
    Rogner().run()


# vim: expandtab shiftwidth=4 tabstop=4 softtabstop=4 fileencoding=utf-8 textwidth=80
