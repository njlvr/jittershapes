#!/usr/bin/env python
'''
Copyright (C) 2012 Juan Pablo Carbajal ajuanpi-dev@gmail.com
Copyright (C) 2005 Aaron Spike, aaron@ekips.org

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
import random, math, inkex
from inkex import paths

LEFT = 0
NODE = 1
RIGHT = 2
X = 0
Y = 1
U = 2
V = 3
# TODO
# N.B. No overlap
# Check effet locked layes
# /!\ Shapes of 2 shapes ? Probably no pb
# dist_* => null

def randomize(rx, ry, dist):
    if dist == "Gaussian" or dist == JitterShapes.dist_gaussian:
        r1 = random.gauss(0.0,rx)
        r2 = random.gauss(0.0,ry)
    elif dist == "Pareto" or dist == JitterShapes.dist_pareto:
        '''
        sign is used ot fake a double sided pareto distribution.
        for parameter value between 1 and 2 the distribution has infinite variance
        I truncate the distribution to a high value and then normalize it.
        The idea is to get spiky distributions, any distribution with long-tails is
        good (ideal would be Levy distribution).
        '''
        sign = random.uniform(-1.0,1.0)

        r1 = min(random.paretovariate(1.0), 20.0)/20.0
        r2 = min(random.paretovariate(1.0), 20.0)/20.0

        r1 = rx * math.copysign(r1, sign)
        r2 = ry * math.copysign(r2, sign)
    elif dist == "Lognorm" or dist == JitterShapes.dist_lognorm:
        sign = random.uniform(-1.0,1.0)
        r1 = rx * math.copysign(random.lognormvariate(0.0,1.0)/3.5,sign)
        r2 = ry * math.copysign(random.lognormvariate(0.0,1.0)/3.5,sign)
    elif dist == "Uniform" or dist == JitterShapes.dist_uniform:
        r1 = random.uniform(-rx,rx)
        r2 = random.uniform(-ry,ry)

    return [r1, r2]

def display(tag, node):
    indent = ""
    if tag:
        inkex.utils.debug(tag + ":")
        indent = "    "
    else:
        inkex.utils.debug('')

    inkex.utils.debug('{}node = [{}, {}]'.format(indent, node[X], node[Y]))
 

def idNode( node ):
    # Identify the NODE
    return 'node[{},{}]'.format( node[ X ], node[ Y ] )

def idHandle( prev, node ):
    # Identify the handle
    return [ 'handle[{}-{}]'.format( idNode( prev ), idNode( node )),
             'handle[{}-{}]'.format( idNode( node ), idNode( prev )) ]

def snap( csp, snap ):
    # Snap NODE
    if snap == 0:
            return csp

    # Snap NODE to a multiple of snap
    # Shift the handles if the same direction
    shift = [ csp[ NODE ][ X ] - round( csp[ NODE ][ X ] / snap ) * snap,
              csp[ NODE ][ Y ] - round( csp[ NODE ][ Y ] / snap ) * snap ]

    for i in LEFT, NODE, RIGHT:
        for xy in X, Y:
            csp[ i ][ xy ] -= shift[ xy ]
    return csp

# https://www.it-swarm-fr.com/fr/java/comment-determiner-si-un-point-est-linterieur-dun-polygone-convexe-2d/940800366/
def inside( node, subpath, debug ):
    if debug:
        display( "\n===inside\nnode", node )
        inkex.utils.debug('subpath: ' + str(subpath))
        inkex.utils.debug('len(subpath: ' + str(len(subpath)))

    # subpath contains 1st element as last element as well
    result = False
    for i in range( 1, len( subpath )):
        j = i - 1
        if debug:
            inkex.utils.debug('i,j: {},{}: '.format( i, j ))
            inkex.utils.debug('subpath-i: ' + str(subpath[i][NODE]))
            inkex.utils.debug('subpath-j: ' + str(subpath[j][NODE]))
            inkex.utils.debug( "1 = {}".format (subpath[i][NODE][Y] > node[Y]))
            inkex.utils.debug( "2 = {}".format (node[X] ))
        diviseur = subpath[j][NODE][Y] - subpath[i][NODE][Y]
        if debug and diviseur != 0:
            inkex.utils.debug( "diviseur = {}".format ( (subpath[j][NODE][Y] - subpath[i][NODE][Y]) ))
            inkex.utils.debug( "3 = {}".format ((subpath[j][NODE][X] - subpath[i][NODE][X]) * (node[Y] - subpath[i][NODE][Y]) / (subpath[j][NODE][Y] - subpath[i][NODE][Y]) + subpath[i][NODE][X]))
            inkex.utils.debug( "4 = {}".format ( node[X] < (subpath[j][NODE][X] - subpath[i][NODE][X]) * (node[Y] - subpath[i][NODE][Y]) / (subpath[j][NODE][Y] - subpath[i][NODE][Y]) + subpath[i][NODE][X]))

        if (((subpath[i][NODE][Y] > node[Y]) != (subpath[j][NODE][Y] > node[Y])) and (node[X] < (subpath[j][NODE][X] - subpath[i][NODE][X]) * (node[Y] - subpath[i][NODE][Y]) / (subpath[j][NODE][Y] - subpath[i][NODE][Y]) + subpath[i][NODE][X])):
            result = not result

    if debug:
        inkex.utils.debug('===' + str( result ) + '=========================================')
    return result


class JitterShapes(inkex.EffectExtension):
    '''Jiggle shapes around'''
    def add_arguments(self, pars):
        pars.add_argument("--reduction",
                        type=int, default=1.0,
                        help="Apply a reduction to each shape individually, respect with existing slopes.")
        pars.add_argument("--debug",
                        type=inkex.Boolean, default=False,
                        help="Debug mode. Ends in i=1/0<br>JZeroDivisionError: division by zero")
        pars.add_argument("--tab",
                        help="The selected UI-tab when OK was pressed")

    def effect(self):
        deltas = {}
        jitter = {}
        M = {}
        # slopem = {}
        _M = {}
        Delta = {}
        delta = {}
        for id, node in iter(self.svg.selected.items()):
            if node.tag == inkex.addNS('path','svg'):
                d = node.get('d')
                if self.options.debug:
                    inkex.utils.debug('\n*** node: ' + str(node.attrib["id"])) # path9999
                # parsePath:: Parse SVG path and return an array of segments.
                # Removes all shorthand notation.
                # Converts coordinates to absolute. <==
                # p = cubicsuperpath.parsePath(d)
                p = paths.CubicSuperPath(paths.Path(d))
                for subpath in p:
                    inkex.utils.debug('subpath: ' + str(subpath))
                    prev = None
                    lines = {}
                    for csp in subpath:
                        if prev:
                            idLine = "line({},{})".format( idNode( prev[ NODE ]), idNode( csp[ NODE ]))
                            if self.options.debug:
                                inkex.utils.debug("###########################")
                                inkex.utils.debug("idLine = {}".format( idLine ))
                            for xy in X, Y:
                                # M: Middle prev - node
                                M[xy] = ( prev[NODE][xy] + csp[NODE][xy] ) / 2
                                # m is line from prev to node
                            if self.options.debug:
                                display( "Middle: M", M )
                            # Obso?
                            for xy in X, Y:
                                _M[ xy ] = {}
                            reduction = 1
                            # Delta[X/Y]: slope ligne prev - csp
                            # Delta[X] = prev[NODE][X] - csp[NODE][X]
                            # Delta[Y] = prev[NODE][Y] - csp[NODE][Y]
                            # Delta[X,Y]: slope ligne prev - csp
                            for xy in X, Y:
                                Delta[xy] = prev[NODE][xy] - csp[NODE][xy]
                                if self.options.debug:
                                    inkex.utils.debug("Delta{} = {}".format( xy,
                                        Delta[xy] ))
                            # M_M: Distance M,M'
                            M_M = math.sqrt( Delta[X] ** 2 +  Delta[Y] ** 2 )
                            if self.options.debug:
                                inkex.utils.debug("M_M = {}".format( M_M ))
                                for xy in X, Y:
                            	    # Delta X/Y entre M et M'
                                    inkex.utils.debug("Delta[{}] = {}".format( xy, Delta[xy] ))
                            delta[X] = reduction * Delta[X] / M_M
                            delta[Y] = reduction * Delta[Y] / M_M
                            if self.options.debug:
                                for xy in X, Y:
                                    inkex.utils.debug("delta[{}] = {}".format( xy, delta[xy] ))
                                inkex.utils.debug("reduction ** 2 = {}".format( delta[X] ** 2 + delta[Y] ** 2 ))

                            # Test which one is "in" the shape
                            # N.B. The movement is perpendicular to m
                            _M[X] = M[X] + delta[Y]
                            _M[Y] = M[Y] - delta[X]
                            if not inside( _M, subpath, self.options.debug ):
                                _M[X] = M[X] - delta[Y]
                                _M[Y] = M[Y] + delta[X]
                            if self.options.debug:
                                display( "_M", _M )

                            # lines: array of lines through _M parallel to m
                            lines[idLine] = {}
                            lines[idLine][X] = _M[X]
                            lines[idLine][Y] = _M[Y]
                            lines[idLine][U] = delta[X]
                            lines[idLine][V] = delta[Y]
                            if self.options.debug:
                                inkex.utils.debug("lines = {}".format( str(lines)))

                        prev = csp

                    if self.options.debug:
                        inkex.utils.debug("-------------- final -----------")
                    tmp = {}
                    idPrevLine = None
                    prev = None
                    for csp in subpath:
                        if idPrevLine:
                            idLine = "line({},{})".format( idNode( prev[ NODE ]), idNode( csp[ NODE ]))
                            if self.options.debug:
                                inkex.utils.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>")
                                inkex.utils.debug("idLine = {}".format( idLine ))
                                inkex.utils.debug("idPrevLine = {}".format( idPrevLine ))
                                inkex.utils.debug("x1 = {}".format( lines[idPrevLine][X] ))
                                inkex.utils.debug("y1 = {}".format( lines[idPrevLine][Y] ))
                                inkex.utils.debug("u1 = {}".format( lines[idPrevLine][U] ))
                                inkex.utils.debug("v1 = {}".format( lines[idPrevLine][V] ))
                                inkex.utils.debug("x2 = {}".format( lines[idLine][X] ))
                                inkex.utils.debug("y2 = {}".format( lines[idLine][Y] ))
                                inkex.utils.debug("u2 = {}".format( lines[idLine][U] ))
                                inkex.utils.debug("v2 = {}".format( lines[idLine][V] ))
                            laambda = ( lines[idLine][U] * ( lines[idPrevLine][Y] - lines[idLine][Y] ) - lines[idLine][V] * ( lines[idPrevLine][X] - lines[idLine][X] )) / ( lines[idPrevLine][U] * lines[idLine][V] - lines[idLine][U] * lines[idPrevLine][V] ) 
                            mu      = ( lines[idPrevLine][U] * ( lines[idLine][Y] - lines[idPrevLine][Y] ) - lines[idPrevLine][V] * ( lines[idLine][X] - lines[idPrevLine][X] )) / ( lines[idLine][U] * lines[idPrevLine][V] - lines[idPrevLine][U] * lines[idLine][V] ) 
                            if self.options.debug:
                                inkex.utils.debug("laambda = {}".format( laambda ))
                                inkex.utils.debug("mu      = {}".format( mu ))

                            idNODE = idNode( csp[ NODE ])
                            tmp[idNODE] = {}
                            tmp[idNODE][X] = lines[idPrevLine][X] + laambda * lines[idPrevLine][U]
                            tmp[idNODE][Y] = lines[idPrevLine][Y] + laambda * lines[idPrevLine][V]
                            if self.options.debug:
                                inkex.utils.debug("tmp = [{},{}]".format( tmp[idNODE][X], tmp[idNODE][Y] ))
                        idPrevLine = idLine
                        prev = csp

                    if self.options.debug:
                        inkex.utils.debug("=========================\ntmp = {}".format( str( tmp )))
                    for csp in subpath:
                        idNODE = idNode( csp[ NODE ])
                        if self.options.debug:
                            display( 'cspNODE', csp[NODE])
                        for n in LEFT, NODE, RIGHT:
                            for xy in X, Y:
                                csp[n][xy] = tmp[idNODE][xy]
                        if self.options.debug:
                            display( 'cspNODE after', csp[NODE])

                node.set('d', str(paths.Path(paths.CubicSuperPath(p).to_path().to_arrays())))

        if self.options.debug:
            i=1/0

    def randomize(self, pos):
        """Randomise the given position [x, y] as set in the options"""
        delta = self.options.dist(self.options.radiusx, self.options.radiusy)
        return [pos[0] + delta[0], pos[1] + delta[1]]

    @staticmethod
    def dist_gaussian(x, y):
        """Gaussian distribution"""
        return random.gauss(0.0, x), random.gauss(0.0, y)

    @staticmethod
    def dist_pareto(x, y):
        """Pareto distribution"""
        # sign is used to fake a double sided pareto distribution.
        # for parameter value between 1 and 2 the distribution has infinite variance
        # I truncate the distribution to a high value and then normalize it.
        # The idea is to get spiky distributions, any distribution with long-tails is
        # good (ideal would be Levy distribution).
        sign = random.uniform(-1.0, 1.0)
        return x * math.copysign(min(random.paretovariate(1.0), 20.0) / 20.0, sign),\
               y * math.copysign(min(random.paretovariate(1.0), 20.0) / 20.0, sign)

    @staticmethod
    def dist_lognorm(x, y):
        """Log Norm distribution"""
        sign = random.uniform(-1.0, 1.0)
        return x * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign),\
               y * math.copysign(random.lognormvariate(0.0, 1.0) / 3.5, sign)

    @staticmethod
    def dist_uniform(x, y):
        """Uniform distribution"""
        # return random.uniform(-x, x), random.uniform(-y, y)

if __name__ == '__main__':
    JitterShapes().run()


# vim: expandtab shiftwidth=4 tabstop=4 softtabstop=4 fileencoding=utf-8 textwidth=80
